import { StyleSheet, Platform } from 'react-native';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#333',
    paddingTop: 0,
    alignItems: 'center',
    justifyContent: 'space-between',
  },

  backgroundImage: {
    width: '100%',
    height: '100%',
    position: 'absolute',
    opacity: 0.7,
  },

  logoContainer: {
    width: '50%',
    maxHeight: 200,
  },

  logo: {
    width: '100%',
    resizeMode: 'contain',
  },

  welcomeMessage: {
    fontFamily: (Platform.OS === 'ios') ? 'Bebas Neue' : 'bebas_bold',
    fontSize: 64,
    color: 'white',
    marginHorizontal: 20,
    lineHeight: 64,
    marginTop: 100,
  },

  footer: {
    width: '100%',
  },

  entrarButton: {
    width: '100%',
    height: 50,
    backgroundColor: '#399748',
    paddingTop: 10,
    alignItems: 'center',
  },

  entrarButtonText: {
    color: 'white',
    fontFamily: (Platform.OS === 'ios') ? 'Bebas Neue' : 'bebas_bold',
    fontSize: 26,
    marginBottom: 7,
  }
});

export default styles;
