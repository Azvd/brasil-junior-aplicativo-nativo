import React, { Component } from 'react';

import { DrawerNavigator } from 'react-navigation';

import SideMenu from '../components/SideMenu';

// Screens
import Home from '../screens/Home/';
import Articles from '../screens/Articles/';
import Hello from '../screens/Hello';

const AppNavigator = DrawerNavigator(
	{
	  Home: {
	    screen: Home,
	  },

	  Articles: {
	  	screen: Articles,
	  },
	},
	{
		initialRouteName: 'Articles',
		headerModer: 'screen',
		drawerWidth: 300,
		contentComponent: SideMenu,
	}
);

export default class Routes extends Component {
  render() {
    return <AppNavigator />;
  }
}