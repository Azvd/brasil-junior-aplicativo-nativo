import React, { Component } from 'react';

import { 
	View,
	Modal,
	Text,
	Image,
	StyleSheet,
	TouchableOpacity,
	AsyncStorage,
	Platform,
	ScrollView,
} from 'react-native';

import ArticleCard from '../ArticleCard';

import styles from './styles';

import Icon from 'react-native-vector-icons/Ionicons';
import HTMLView from 'react-native-htmlview';

export default class ArticleShow extends Component {
	constructor(props) {
		super(props);

		this.state = {
			visible: props.visible,
			article: props.article,
			categories: props.categories,
		};
	}

	componentWillUnmount() {
		this.setState({ visible: false, article: '', categories: [], });
	}

	_checkCategory(category) {
		if (category.slug === 'mercado') {
			return (
				<View key={category.name} style={[styles.articleCategory, styles.mercadoCategory]}>
					<Text style={styles.articleCategoryText}>
						{ category.name.toUpperCase() }
					</Text>
				</View>
			)
		} else if (category.slug === 'pessoas') {
			return (
				<View key={category.name} style={[styles.articleCategory, styles.pessoasCategory]}>
					<Text style={styles.articleCategoryText}>
						{ category.name.toUpperCase() }
					</Text>
				</View>
			)
		} else if (category.slug === 'execucao') {
			return (
				<View key={category.name} style={[styles.articleCategory, styles.execucaoCategory]}>
					<Text style={styles.articleCategoryText}>
						{ category.name.toUpperCase() }
					</Text>
				</View>
			)
		} else {
			return (
				<View key={category.name} style={styles.articleCategory}>
					<Text style={styles.articleCategoryText}>
						{ category.name.toUpperCase() }
					</Text>
				</View>
			)
		}
	}

	unloadModal() {
		this.setState({ visible: false });
	}

  render() {
    return (
      <Modal 
      	animationType="slide"
      	presentationStyle={'overFullScreen'}
      	visible={this.props.visible}
      	onRequestClose={this.componentWillUnmount && this.props.onCancel}
      >
      	<View style={styles.modalContainer}>
      		<ScrollView contentContainerStyle={styles.articleList}>
	      		<Image
	      			style={styles.articleImage}
	      			source={{uri: this.props.article.image}}
	      		/>
	      		<TouchableOpacity
							onPress={this.props.onCancel} >
							<Icon style={styles.arrowBackIcon} size={40} name={(Platform.OS === 'ios') ? 'ios-arrow-back' : 'md-arrow-back'} />
						</TouchableOpacity>
	      		<View style={styles.articleInfo}>
	      			<View style={styles.articleCategoriesDate}>
	      				<View style={styles.articleCategories}>

									{ this.props.categories.map(
											category =>
												this._checkCategory(category)
										)
									}

	      				</View>
	      				<Text style={styles.articlePostTime}>{this.props.article.date}</Text>    				
	      			</View>

	      		</View>
	      		<Text style={styles.articleTitle}>{this.props.article.title}</Text>      			

      			<View style={styles.articleContentContainer}>
	      			<HTMLView
				        value={this.props.article.content}
				        stylesheet={styles}
				        addLineBreaks={false}
	      		  />
      		  </View>
					</ScrollView>      		
      	</View>
      </Modal>
    );
  }
}
