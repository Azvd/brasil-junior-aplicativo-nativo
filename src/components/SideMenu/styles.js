import { StyleSheet, Platform } from 'react-native';

const styles = StyleSheet.create({
	container: {
		backgroundColor: '#1E6085',
		height: '100%',
	},

  header: {
  	height: (Platform.OS === 'ios') ? 98 : 70,
    flexDirection: 'row',
    paddingHorizontal: 20,
    paddingRight: 50,

    paddingTop: (Platform.OS === 'ios') ? 30 : 8,
    justifyContent: 'flex-start',
  },

  avatar: {
    height: 50,
    width: 50,
    borderRadius: 25
  },

  userInfo: {
    paddingLeft: 10,
  },

  userName: {
    fontFamily: (Platform.OS === 'ios') ? 'Bebas Neue' : 'bebas_bold',
    color: 'white',
    fontSize: 24,
    fontWeight: "normal"
  },

  userEj: {
    fontFamily: (Platform.OS === 'ios') ? 'Bebas Neue' : 'bebas_bold',
    color: 'white',
    fontSize: 16,
    fontWeight: "100",
  },

  navSection: {
    marginTop: (Platform.OS === 'ios') ? -1 : -4,
  },

  navItemFirst: {
    height: 50,
    justifyContent: 'center',
    borderRightWidth: 0,
    borderLeftWidth: 0,
    borderTopWidth: 1,
    borderBottomWidth: 1,
    borderColor: 'white'
  },

  navItem: {
    height: 50,
    justifyContent: 'center',
    borderRightWidth: 0,
    borderLeftWidth: 0,
    borderBottomWidth: 1,
    borderColor: 'white'
  },

  navItemText: {
    fontFamily: (Platform.OS === 'ios') ? 'Bebas Neue' : 'bebas_bold',
    fontSize: 20,
    color: 'white',
    paddingLeft: 20,
  },

  sairButton: {
    width: '100%',
    height: 50,
    backgroundColor: '#399748',
    paddingTop: 10,
    alignItems: 'flex-start',
    justifyContent: 'center',
  },

  sairButtonText: {
    color: 'white',
    fontFamily: (Platform.OS === 'ios') ? 'Bebas Neue' : 'bebas_bold',
    fontSize: 26,
    marginBottom: 7,
    paddingLeft: 20,
  }

});

export default styles;
