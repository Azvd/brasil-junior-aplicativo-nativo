import { StyleSheet, Platform } from 'react-native';

const styles = StyleSheet.create({
  header: {
    height: (Platform.OS === 'ios') ? 90 : 59,
    backgroundColor: '#227DBF',
    flexDirection: 'row',
    paddingHorizontal: 20,
    paddingRight: 50,

    paddingTop: (Platform.OS === 'ios') ? 25 : 0,
    justifyContent: 'space-between'
  },

  subHeader: {
    height: 8,
    backgroundColor: '#1E6085',
  },

  menuIcon: {
    paddingTop: (Platform.OS === 'ios') ? 10 : 11,
    color: '#EFEFEF',
  },

  logo: {
    height: 50,
    width: 80,
    marginTop: 5,
    marginLeft: 'auto',
    marginRight: 'auto',
  },
});

export default styles;
