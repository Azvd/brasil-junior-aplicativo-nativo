import React, { Component } from 'react';

import {
  StyleSheet,
  View,
  AsyncStorage,
  FlatList
} from 'react-native';

import Icon from 'react-native-vector-icons/Ionicons';

import styles from './styles';

// Components of the screen
import Header from '../../components/Header';
import ArticleCard from '../../components/Article/ArticleCard';

// Config with all environment variables
import Config from 'react-native-config';

export default class Articles extends Component {
  state = {
    articles: [],
    page: 1,
    refreshing: false,
  };

  async componentDidMount() {
    this.loadArticles();
  }

  loadArticles = async () => {
    if (this.state.refreshing) return;

    this.setState({ refreshing: true });

    const response = await fetch(`${Config.API_URL}?page=${this.state.page}`);
    const articles = await response.json();

    this.setState({
      articles: [ ...this.state.articles, ...articles],
      page: this.state.page + 1,
      refreshing: false,
    });
  }

  async reloadArticles() {
    this.loadArticles();
  }

  renderItem = ({ item }) => (
    <ArticleCard data={item} />
  );

  render() {
    return (
      <View style={styles.container}>
        <Header 
          navigation={this.props.navigation}
        />

        <FlatList
          contentContainerStyle={styles.articleList}
          data={this.state.articles}
          renderItem={this.renderItem}
          keyExtractor={item => item.id.toString()}
          onEndReached={this.loadArticles}
          onEndReachedThreshold={0.1}
        />

      </View>
    );
  }
}