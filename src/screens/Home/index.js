import React, { Component } from 'react';

import {
  StyleSheet,
  Text,
  View,
  Image,
  TouchableOpacity,
} from 'react-native';

import Icon from 'react-native-vector-icons/Ionicons';

import styles from './styles';

export default class Home extends Component {
  render() {
    return (
      <View style={styles.container}>
      <Image
        style={styles.backgroundImage}
        source={require('../../../assets/images/bg.png')}
        blurRadius={5}
      />

        <View style={styles.logoContainer}>
          <Image
            style={styles.logo}
            source={require('../../../assets/images/logo2.png')}
          />
        </View>
        <View style={styles.welcomeMessageContainer}>
          <Text style={styles.welcomeMessage} >A BRASIL JÚNIOR SEMPRE NA PALMA DA SUA MÃO</Text>
        </View>
        <View style={styles.footer}>
          <TouchableOpacity
            onPress={() => this.props.navigation.navigate('Articles')}
            style={styles.entrarButton}
          >
            <Text style={styles.entrarButtonText}>ENTRAR</Text>
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}