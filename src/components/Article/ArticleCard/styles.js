import { StyleSheet, Platform } from 'react-native';

const styles = StyleSheet.create({
  articleCard: {
		backgroundColor: '#FFF',
		justifyContent: 'space-between',
		marginHorizontal: 10,
		marginTop: 10,
		marginBottom: 10,
		shadowColor: '#000',
		shadowOpacity: 0.2,
		shadowOffset: {width: 0,  height: 1},
		elevation: 1,
	},

	articleCardImage: {
		minHeight: 300,
		minWidth: undefined,
	},

	articleCardInfo: {
		padding: 10,
	},

	articleCardArticleCategoriesDate: {
		flexDirection: 'row',
		justifyContent: 'space-between',
	},

	articleCardCategories: {
		flexDirection: 'row',
		alignItems: 'center',
	},

	articleCategory: {
		backgroundColor: '#3F3F3F',
		borderRadius: 2,
		paddingTop: 2,
		paddingBottom: 2,
		paddingRight: 3,
		paddingLeft: 3,
		marginRight: 5,
	},

	mercadoCategory: {
		backgroundColor: '#3A9747',
	},

	pessoasCategory: {
		backgroundColor: '#FEC02C',
	},

	execucaoCategory: {
		backgroundColor: '#267FC0',
	},

	articleCategoryText: {
		fontSize: 8,
		fontWeight: 'bold',
		color: 'white',
	},

	articleCardPostTime: {
		alignSelf: 'flex-end',
		fontFamily: (Platform.OS === 'ios') ? 'Bebas Neue' : 'bebas_bold',
		color: '#AFAFAF',
	},

	articleCardTitle: {
		fontFamily: (Platform.OS === 'ios') ? 'Bebas Neue' : 'bebas_bold',
		fontSize: 24,
		color: '#3F3F3F',
		justifyContent: 'flex-start',
		lineHeight: 26,
		marginTop: 10,
	}
});

export default styles;
