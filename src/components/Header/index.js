import React, { Component } from 'react';

import { 
	View,
	TouchableOpacity, 
	Image,
	Platform,
} from 'react-native';

import Icon from 'react-native-vector-icons/Ionicons';

import styles from './styles';

export default class Header extends Component {
	constructor(props) {
		super(props);

		this.state = {
			navigation: props.navigation,
		};
	}

  render() {
    return (
    	<View>
	      <View style={styles.header}>
	      	<TouchableOpacity onPress={() => this.props.navigation.navigate('DrawerOpen')}>
	          <Icon style={styles.menuIcon} size={40} name={(Platform.OS === 'ios') ? 'ios-menu' : 'md-menu'} />
	        </TouchableOpacity>

	        <Image
	          style={styles.logo}
	          source={require('../../../assets/images/logo.png')}
	        />
	      </View>
	      <View style={styles.subHeader}></View>
      </View>
    );
  }
}
