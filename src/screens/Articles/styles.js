import { StyleSheet, Platform } from 'react-native';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#EFEFEF',
    paddingTop: 0,
  },

  articleList: {
    backgroundColor: '#EFEFEF',
  }
});

export default styles;
