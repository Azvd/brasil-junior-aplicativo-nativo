import { StyleSheet, Platform } from 'react-native';

const styles = StyleSheet.create({
	p: {
		color: '#3F3F3F',
	},

  modalContainer: {
		backgroundColor: '#EFEFEF',
		justifyContent: 'space-between',
   	marginTop: (Platform.OS === 'ios') ? 20 : 0,
		shadowColor: '#000',
		shadowOpacity: 0.2,
		shadowOffset: {width: 0,  height: 1},
		elevation: 1,
	},

	arrowBackIcon: {
		color: 'white',
		paddingTop: 10,
		paddingLeft: 20,
		marginTop: -300,
		position: 'absolute',
	},

	articleImage: {
		height: 300,
		width: '100%',
	},

	articleInfo: {
		paddingTop: 10,
		marginHorizontal: 20,
	},

	articleCategoriesDate: {
		flexDirection: 'row',
		justifyContent: 'space-between',
	},

	articleCategories: {
		flexDirection: 'row',
		alignItems: 'center',
	},

	articleCategory: {
		backgroundColor: '#3F3F3F',
		borderRadius: 2,
		paddingTop: 2,
		paddingBottom: 2,
		paddingRight: 3,
		paddingLeft: 3,
		marginRight: 5,
	},

	mercadoCategory: {
		backgroundColor: '#3A9747',
	},

	pessoasCategory: {
		backgroundColor: '#FEC02C',
	},

	execucaoCategory: {
		backgroundColor: '#267FC0',
	},

	articleCategoryText: {
		fontSize: 8,
		fontWeight: 'bold',
		color: 'white',
	},

	articlePostTime: {
		alignSelf: 'flex-end',
		fontFamily: (Platform.OS === 'ios') ? 'Bebas Neue' : 'bebas_bold',
		color: '#AFAFAF',
	},

	articleTitle: {
		fontFamily: (Platform.OS === 'ios') ? 'Bebas Neue' : 'bebas_bold',
		fontSize: 24,
		color: '#3F3F3F',
		justifyContent: 'flex-start',
		lineHeight: 26,
		marginTop: 10,
		marginHorizontal: 20,
	},

	articleContentContainer: {
		marginHorizontal: 20,
		marginTop: 5,
	},

	articleContent: {
		color: '#3f3f3f',
		height: 200,
		width: '100%',
	}
});

export default styles;
