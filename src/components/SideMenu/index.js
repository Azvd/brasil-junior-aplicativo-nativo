import React, { Component } from 'react';

import { 
	View,
	TouchableOpacity, 
	Image,
	Platform,
	Text,
	ScrollView
} from 'react-native';

import { NavigationActions } from 'react-navigation';

import Icon from 'react-native-vector-icons/Ionicons';

import styles from './styles';

export default class SideMenu extends Component {
	navigateToScreen = (route) => () => {
	  const navigateAction = NavigationActions.navigate({
	    routeName: route
	  });
	  this.props.navigation.dispatch(navigateAction);
	}

	render() {
	  return (
	    <View style={styles.container}>
	    	<View style={styles.header}>
	    		<Image
	    			style={styles.avatar}
	    			source={require('../../../assets/images/avatar.png')}
	    		/>
	    		<View style={styles.userInfo}>
		    		<Text style={styles.userName} onPress={this.navigateToScreen('Articles')}>
		          Luan Santos
	          </Text>
		    		<Text style={styles.userEj} onPress={this.navigateToScreen('#')}>
		          Tootz
	          </Text>
          </View>
	      </View>

	      <ScrollView
	      	style={styles.navSection}
	      	scrollEnabled={false}
	      >
	      	<View style={styles.navItemFirst}>
	      		<Text style={styles.navItemText} onPress={this.navigateToScreen('Articles')}>
		          Notícias
	          </Text>
          </View>

	      	<View style={styles.navItem}>
	      		<Text style={styles.navItemText} onPress={this.navigateToScreen('#')}>
		          Pesquisas
	          </Text>
          </View>
	      </ScrollView>

        <View style={styles.footerContainer}>
        	<TouchableOpacity
            onPress={() => this.props.navigation.navigate('Home')}
            style={styles.sairButton}
          >
            <Text style={styles.sairButtonText}>SAIR</Text>
          </TouchableOpacity>
        </View>
	    </View>
	  );
	}
}