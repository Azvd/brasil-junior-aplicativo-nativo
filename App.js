import React, { Component } from 'react';

import { StackNavigator } from 'react-navigation';

import Routes from './src/routes/Routes';

export default class App extends Component {
  render() {
    return(
    	<Routes />
    );
  }
}