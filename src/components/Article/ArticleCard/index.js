import React, { Component } from 'react';

import { 
	View,
	Text, 
	Image, 
	StyleSheet,
	TouchableOpacity,
	AsyncStorage,
} from 'react-native';

import ArticleShow from '../ArticleShow/';

import styles from './styles';

import Config from 'react-native-config';

export default class ArticleCard extends Component<Props> {
	constructor(props) {
		super(props);

		this.state = {
			article: [],
			modalVisible: false,
		};
	}

	async loadModal(card) {
		card.setState({ modalVisible: true });
		fetch(`${Config.API_URL}artigos/${card.props.data.slug}`)
			.then(response => response.json())
			.then(data => card.setState({ article: data }));
	}

	_checkCategory(category) {
		if (category.slug === 'mercado') {
			return (
				<View key={category.name} style={[styles.articleCategory, styles.mercadoCategory]}>
					<Text style={styles.articleCategoryText}>
						{ category.name.toUpperCase() }
					</Text>
				</View>
			)
		} else if (category.slug === 'pessoas') {
			return (
				<View key={category.name} style={[styles.articleCategory, styles.pessoasCategory]}>
					<Text style={styles.articleCategoryText}>
						{ category.name.toUpperCase() }
					</Text>
				</View>
			)
		} else if (category.slug === 'execucao') {
			return (
				<View key={category.name} style={[styles.articleCategory, styles.execucaoCategory]}>
					<Text style={styles.articleCategoryText}>
						{ category.name.toUpperCase() }
					</Text>
				</View>
			)
		} else {
			return (
				<View key={category.name} style={styles.articleCategory}>
					<Text style={styles.articleCategoryText}>
						{ category.name.toUpperCase() }
					</Text>
				</View>
			)
		}
	}

	render() {
		return (
			<View style ={styles.container}>
				<TouchableOpacity onPress={() => this.loadModal(this)}>
					<View style={styles.articleCard}>
						<Image
							style={styles.articleCardImage}
							source={{ uri: this.props.data.image }}
							// source={require('../../../../assets/image.png')}
						/>
						<View style={styles.articleCardInfo}>
							<View style={styles.articleCardArticleCategoriesDate}>
								<View style={styles.articleCardCategories}>

									{ this.props.data.categories.map(
											category =>
												this._checkCategory(category)
										)
									}

								</View>
								<Text style={styles.articleCardPostTime}>{ this.props.data.date }</Text>
							</View>
							<Text style={styles.articleCardTitle}>{ this.props.data.title }</Text>
						</View>
					</View>
				</TouchableOpacity>

				<ArticleShow
					onCancel={() => this.setState({ modalVisible: false })}
					visible={this.state.modalVisible}
					article={this.state.article}
					categories={this.props.data.categories}
				/>
			</View>

		);
	}
}