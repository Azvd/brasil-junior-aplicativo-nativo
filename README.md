# React-Native app

Init the application:
```
react-native init AppName
```

Get the create file.

In the app's folder, run:
```
./create
```

Configure what needs to be configured for the dependencies and be happy.

##Folder structure (CRS)

```
- src
	|
	| - components
	| 	|_ ComponentName
	| 	|  |_ index.js
	| 	|  |_ styles.js
	|	|
	| 	|_ ParentComponentName
	| 	|	 |_ ChildComponentName
	| 	|	 	|_ index.js
	| 	|  		|_ styles.js
	|
	| - routes
	| 	|_ Routes.js
	|
	| - screens
	| 	|_ ScreenName
	| 	   |_ index.js
	| 	   |_ styles.js
```

##Dependencies:

- react-navigation
- react-native-vector-icons
- react-native-config

##Optional:
We recomend installing the RocketSeat React-Native snippets:

https://github.com/RocketSeat/rocketnative-sublime-snippets
